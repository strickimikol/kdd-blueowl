import fontawesome from '@fortawesome/fontawesome'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas, faOtter } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

fontawesome.library.add(far, fab, fas)

jQuery(document).ready(function ( $ ) {
  // smooth scroll to ID
  $('.oprawa-line-case').on('click', function (e) {
    e.preventDefault();

    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top - 180 // choose what you need
    }, 500, 'linear');
  });

  $('.menu-toggle').on('click', function () {
    ($('.mobile-nav--dropdown').hasClass("is-active")) ? $('.mobile-nav--dropdown').removeClass("is-active"): $('.mobile-nav--dropdown').addClass("is-active");
    
  });

  $('.menu-item-has-children').on('click', function () {
    ($('.sub-menu').hasClass("active")) ? $('.sub-menu').removeClass("active"): $('.sub-menu').addClass("active");
    
  });
});

window.addEventListener('scroll', function(e) {
  const miniNav = document.querySelector('.oprawa-lines');
  const miniNavOffsetTop = miniNav.offsetTop;

  console.log(miniNavOffsetTop)

  if(window.scrollY > 724) {
    miniNav.style.top = '100px';
    miniNav.style.position = 'sticky';
  } else {
    miniNav.style.top = '100px';
    miniNav.style.position = 'static';

  }
});

// // Function for mobile-menu

// function dropdown() {
//   var x = document.getElementByClassName("sub-menu");

//   if(x.style.display === "block") {
//     x.style.display = "none";
//   } else {
//     x.style.display = "block";
//   }

// }

