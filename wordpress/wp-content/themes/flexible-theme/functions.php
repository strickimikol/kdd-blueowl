<?php

// include PHP files
require get_template_directory() . '/includes/metaboxes.php';
require get_template_directory() . '/includes/widgets.php';
require get_template_directory() . '/includes/enqueue.php';
require get_template_directory() . '/includes/security.php';
require get_template_directory() . '/includes/acfprosettings.php';
require get_template_directory() . '/includes/polylang.php';

// theme support
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_image_size('fullHD', 1920);

// register menus
register_nav_menus( array(
	'header_menu' => __( 'Header Menu' ),
	'footer_menu' => __( 'Footer Menu' )
));

// custom pagination
function custom_pagination( $posts ) {

	$links = paginate_links(
	  array(
		'mid_size' => 2,
		'current' => max( 1, get_query_var('paged') ),
		'total' => $posts->max_num_pages,
		'prev_next' => true,
		'prev_text' => __( '&larr; Prev' ),
		'next_text' => __( 'Next &rarr;' ),
		'type'      => 'array',
	  )
	);
	?>
	<nav aria-label="Posts navigation">
	  <ul class="pagination">
		<?php
		  if ( $links ) {
			foreach ( $links as $key => $link ) { ?>
			  <li class="page-item <?php echo strpos( $link, 'current' ) ? 'active' : '' ?>">
				  <?php echo str_replace( 'page-numbers', 'page-link', $link ); ?>
			  </li>
		<?php
			}
		  }
		?>
	  </ul>
	</nav>
	<?php

function wpb_image_editor_default_to_gd( $editors ) {
    $gd_editor = 'WP_Image_Editor_GD';
    $editors = array_diff( $editors, array( $gd_editor ) );
    array_unshift( $editors, $gd_editor );
    return $editors;
}
add_filter( 'wp_image_editors', 'wpb_image_editor_default_to_gd' );
  }

  function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');
