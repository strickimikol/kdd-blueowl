<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php wp_head(); ?>

    <?php if ( is_admin_bar_showing() ) {?>
     <style>
        @media (min-width: 0px) {
            #header { top: 46px !important; }
        }
        @media (min-width: 768px) {
            #header { top: 32px !important; }
        }
     </style>
    <?php
    } else { ?>
     <style>
      #header { top: 0 !important; }
     </style>
    <?php } ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    
</head>
<body <?php body_class(); ?>>

    <header id="header">

        <?php $main_setting = get_field('main_setting', 'site-settings');?>

        <div class="headerDesktop__wrapper">
        
            <nav class="navbar" role="navigation">
            
                <div class="container">
                
                    <div class="main-nav">
                    
                        <a class="main-logo" href="<?php echo get_home_url();?>"><img src="<?php echo $main_setting['logo_icon'];?>"></a>
                            <?php wp_nav_menu(array(
                            'menu'              => 'main manu',
                            'theme_location'    => 'header_menu',
                            'depth'             => 0,
                            'container_class'   => 'header__nav__menu__wrapper',
                            'menu_class'        => 'menu'
                            )); ?>

                        <div class="cart-and-toggleMenu">
                        
                            <div class="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>

                        </div>

                    
                    </div>

                    <div class="mobile-nav">
                    
                        <a class="mobile-logo" href=" <?php echo get_home_url();?>"><img src="<?php echo $main_setting['logo_icon'];?>"></a>
                        
                        <div class="cart-and-toggleMenu">
                        
                            <div class="menu-toggle" ><i class="fa fa-bars" aria-hidden="true"></i></div>

                        </div>

                        <div class="mobile-nav--dropdown">
                        
                            <div class="mobile-nav--inside">
                                <?php 
                                    wp_nav_menu(array(
                                        'menu'          => 'header_menu',
                                        'depth'         => 2
                                    ));
                                ?>
                            </div>
                        
                        </div>
                    
                    </div>
                
                </div>
            
            </nav>

        </div>
        
    </header>