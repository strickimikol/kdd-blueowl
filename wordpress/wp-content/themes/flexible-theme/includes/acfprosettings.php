<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
        'page_title'    =>  'Ustawienia strony',
        'menu_title'    =>  'Ustawienia strony',
        'menu_slug'     =>  'main-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  '',
        'position'      =>  false,
        'icon_url'      =>  false,
    ));

    acf_add_options_sub_page(array(
        'page_title'    =>  'Ustawienia stałe',
        'menu_title'    =>  'Ustawienia stałe',
        'menu_slug'     =>  'site-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        'post_id'       => 'site-settings'
    ));

    acf_add_options_sub_page(array(
        'page_title'    =>  'Sekcja Kontakt',
        'menu_title'    =>  'Sekcja Kontakt',
        'menu_slug'     =>  'contact-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        'post_id'       => 'contact-settings'
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    =>  'Sekcja Stopka',
        'menu_title'    =>  'Sekcja Stopka',
        'menu_slug'     =>  'footer-settings',
        'capability'    =>  'edit_posts',
        'parent_slug'   =>  'main-settings',
        'position'      =>  false,
        'icon_url'      =>  false,
        'post_id'       => 'footer-settings'
    ));
}


