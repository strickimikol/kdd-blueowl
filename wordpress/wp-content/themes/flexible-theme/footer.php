<?php 
	$main_footer = get_field('main-footer', 'footer-settings');
?>
	
	<footer class="mt-5"> 

		<div class="footer-box">
			
		<div class="footer-wrapper">

				<div class="footer-column-1">
					<img src="<?php echo $main_footer['column_1']['image']; ?>" alt="">
					<div class="footer-content">
						<?php echo $main_footer['column_1']['wysiwyg']; ?>
					</div>
				</div>

				

				<div class="footer-column-2">
					<ul>
						<?php foreach($main_footer['column_2']['link_list'] as $item) : ?>
							<li>
								<a href="<?php echo $item['link']; ?>">
									
										<?php echo $item['text']; ?>
									
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				
				<div class="footer-column-3">
					<h4 class="footer-header"><?php pll_e('Stopka tytul'); ?></h4>
					<p class="footer-description"><?php pll_e('Stopka opis'); ?></p>
				</div>
				

			</div>
		</div>

		<div class="under-footer">

			<div class="designed-by">Designed by Blue Owl</div>

		</div>

		<?php wp_footer(); ?>
	</body>
</html>
