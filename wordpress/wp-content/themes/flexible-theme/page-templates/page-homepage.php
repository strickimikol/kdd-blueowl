 <?php
/**
 * Template Name: Homepage
 *
 */

 get_header();

?>


<div class="wrapper">

<?php $main_section = get_field('homepage');?>

<!-- Homepage-top section with header and description -->
<section class="homepage" id="up" style="background-image: url('<?php echo $main_section['background_homepage']?>')" >

    <div class="img-homepage">

        <img src="<?php echo $main_section['img_homepage'];?>"/> 

    </div>

    <div class="text-homepage-top">

        <h2 class="header-homepage"><?php echo $main_section['header_homepage'];?></h2>

        <h4 class="description-homepage"> <?php echo $main_section['description_homepage'];?> </h4>

    </div>

</section>

<!-- Section with 4 features under homepage-top section -->
<section class="under-header">

    <?php foreach($main_section['feature'] as $item): ?>
    
        <div class="feature" style="background-color:<?php echo $item['feature_back_color'];?>;">

            <div class="feature-image"><img src="<?php echo $item['feature_img']?>"/></div>

            <div class="feature-header"><?php echo $item['feature_head'];?></div>

            <p class="feature-desc" ><?php echo $item['feature_desc'];?></p>

        </div>

    <?php endforeach;?>

</section>

<!-- Video YT + text section '56.25%' -->
<section class="about-us">


    <?php $number = 0;?>

    <?php foreach($main_section['about_us'] as $about): ?>

        <?php $number = $number + 1;?>

      

            <div class="about-us-container ">

                
                <div class="about-us-img-relative"></div>
                

                <div class="about-us-text">
                    <h2 class="about-us-header"><?php echo $about['about_header'];?></h2>
                    <h4 class="about-us-desc"><?php echo $about['about_desc'];?></h4>
                </div>

                <div  class="about-us-vid">
                    <div class="about-us-vid-container even-video">
                        <?php echo $about['about_link'];?>
                    </div>
                </div> 

            </div>

            
    <?php endforeach;?>
        
</section>

<!-- Questions section -->
<section class="questions">

    <div class="questions-text">
        <h2 class="questions-header"><?php echo $main_section['questions_group']['questions_header']?></h2>
        <div class="questions-description"><?php echo $main_section['questions_group']['questions_description']?></div>
    </div>

    <div class="questions-profile">

        <div class="questions-profile-image">
        
            <div class="img-1"><img src="<?php echo $main_section['questions_group']['questions_profile_image_1']?>"/></div>
            <div class="img-2"><img src="<?php echo $main_section['questions_group']['questions_profile_image_2']?>"/></div>

        </div>
        
        <div class="profile-info">

            <h2 class="profile-title"><?php echo $main_section['questions_group']['profile_thing']?></h2>
            <div class="profile-data">tel: <a href="tel:<?php echo $main_section['questions_group']['profile_number']?>" class="link"><?php echo $main_section['questions_group']['profile_number']?></a></div>
            <div class="profile-data">mail: <a href="tel:<?php echo $main_section['questions_group']['profile_mail']?>" class="link"><?php echo $main_section['questions_group']['profile_mail']?></a></div>

        </div>

    </div>

</section>

<!-- Our life section -->
<section class="life">

    <div class="life-img">

        <div class="life-img-in"><img src="<?php echo $main_section['img_life'];?>"/></div>

    </div>

    <div class="life-text">

        <h3><?php echo $main_section['text_life']?></h3>

    </div>

</section>

<!-- Last section on homepage -->
<section class="last-one" style="background-image: url('<?php echo $main_section['last_background'];?>')">

    <div class="last-image">

        <img src="<?php echo $main_section['last_image'];?>"/> 
        
    </div>

    <div class="last-text">

        <h2 class="last-header"><?php echo $main_section['last_header'];?></h2>
        <div class="last-description"><?php echo $main_section['last_description'];?></div>

        <?php if($main_section['last_link']): ?>

            <div class="last-read-border" onclick="location.href='<?php echo $main_section['last_link'];?>'">

                <button onclick="location.href='<?php echo $main_section['last_link'];?>'" class="last-read">czytaj więcej</button>

            </div>

        <?php endif; ?>

    </div>

    <div class="last-arrow">

        <a href="#up"><img class="arrow-up" src="<?php echo $main_section['last_arrow'];?>"/></a> 

    </div>
    

</section>

<?php get_footer(); ?>
