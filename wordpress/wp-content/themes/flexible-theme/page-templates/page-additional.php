<?php
/**
 * Template Name: Dodatkowe
 *
 */

 get_header();

?>

<div class="wrapper">
<?php $additional = get_field('additional_group');?>
<!-- additional-top section with header and background image -->
<section class="additional" style="background-image: url('<?php echo $additional['additional_background'];?>')">

    <img class="img-additional" src="<?php echo $additional['img_additional']?>"/>
    <div class="additional-header"><?php echo $additional['additional_header'];?></div>
    
</section>

<!-- Content of additional page with image and some description to it + file -->
<section class="additional-content">

    <div class="additional-content-image">

        <img src="<?php echo $additional['content_image']?>"/>

    </div>

    <ul class="additional-content-list">
        <?php foreach($additional['content_repeater'] as $item): ?>

            <li class="additional-content-list-value"><span><?php echo $item['content_value'];?></span></li>
            
        <?php endforeach;?>
    </ul>

</section>

<?php get_footer(); ?>