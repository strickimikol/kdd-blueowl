<?php
/**
 * Template Name: Przygotowanie
 *
 */

 get_header();

?>

<div class="wrapper">
<?php $przygotowanie = get_field('przygotowanie_group');?>
<!-- Przygotowanie-top section with header and background image -->
<section class="przygotowanie" style="background-image: url('<?php echo $przygotowanie['przygotwanie_background'];?>')">

    <img class="przygotowanie-img" src="<?php echo $przygotowanie['przygotowanie_img'];?>"/>
    <div class="przygotowanie-header"><?php echo $przygotowanie['przygotowanie_header'];?></div>

</section>

<!-- Content of przygotowanie page with image and some description to it + file -->
<section class="przygotowanie-content">

    <div class="przygotowanie-content-image">

        <img src="<?php echo $przygotowanie['content_image'];?>"/>

    </div>
    <div class="przygotowanie-content-text">

        <?php echo $przygotowanie['content_text'];?>
         

        <a class="przygotowanie-content-file" href="<?php echo $przygotowanie['content_file'];?>"><p>TUTAJ</p></a><p>mogą Państwo pobrać nasze szczegółowe wytyczne dotyczące przygotowywania plików do druku.</p>

    </div>

</section>

<?php get_footer(); ?>