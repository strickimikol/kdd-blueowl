<?php
/**
 * Template Name: Druk
 *
 */

 get_header();

?>

<div class="wrapper">
<?php $druk = get_field('druk_group');?>
<!-- Druk-top section with header and background image -->
<section class="druk" style="background-image: url('<?php echo $druk['druk_background'];?>')">

    <img class="img-druk" src="<?php echo $druk['img_druk']?>"/>
    <div class="druk-header"><?php echo $druk['druk_header'];?></div>

</section>

<!-- Content of druk page with image and some description to it + file -->
<section class="druk-content">

    <div class="druk-content-image">

        <img src="<?php echo $druk['content_image']?>"/>

    </div>
    <div class="druk-content-text">

        <?php echo $druk['content_text'];?>

        <a class="druk-content-file" href="<?php echo $druk['content_file'];?>"><p>TUTAJ</p></a><p>mogą Państwo pobrać nasze szczegółowe wytyczne dotyczące przygotowywania plików do druku.</p>

        

    </div>

</section>

<?php get_footer(); ?>