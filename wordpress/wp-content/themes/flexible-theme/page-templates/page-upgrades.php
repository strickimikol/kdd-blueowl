<?php
/**
 * Template Name: Uszlachetnienia
 */

 get_header();

?>

<div class="wrapper">
<?php $upgrades = get_field('upgrades_group');?>
<!-- Upgrades-top section with header and background image -->
<section class="upgrades" style="background-image: url('<?php echo $upgrades['upgrades_background'];?>')">

    <img class="img-upgrades" src="<?php echo $upgrades['img_upgrades']?>"/>
    <div class="upgrades-header"><?php echo $upgrades['upgrades_header'];?></div>

</section>

<!-- Content of upgrades page with image and some description to it + file -->
<section class="upgrades-content">

    <div class="upgrades-content-image">

        <img src="<?php echo $upgrades['content_image']?>"/>

    </div>
    
    <ul class="upgrades-content-list">
        <?php foreach($upgrades['content_repeater'] as $item): ?>

            <li class="upgrades-content-list-value"><span><?php echo $item['content_value'];?></span></li>
            
        <?php endforeach;?>
    </ul>


</section>

<?php get_footer(); ?>