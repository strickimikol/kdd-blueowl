<?php
/**
 * Template Name: Kontakt
 *
 */

 get_header();
?>

<?php $kontakt = get_field('kontakt');?>
<div class="wrapper">

<!-- The beginning of section with header and top image on page kontakt. -->
<section class="kontakt" style="background-image: url('<?php echo $kontakt['kontakt_background']?>')">

    <div class="kontakt-img">

        <img src="<?php echo $kontakt['kontakt_header_img'];?>"/>

    </div>

    <div class="kontakt-header">

        <h2 class="kontakt-header-text"><?php echo $kontakt['kontakt_header'];?></h2>
        
    </div>

</section>

<!-- The beginnig of section with informations about tel number and mail on page kontakt. -->
<section class="kontakt-info">

    <div class="kontakt-info-telefon">

        <div class="kontakt-info-telefon-img">
            <img src="<?php echo $kontakt['kontakt_info']['kontakt_info_telefon_img'];?>"/>
        </div>

        <div class="kontakt-info-telefon-text">

            <h2>Telefon</h2>

            <p class="kontakt-info-telefon-text-desc"><?php echo $kontakt['kontakt_info']['kontakt_info_telefon_desc']?></p>

            <p><?php echo $kontakt['kontakt_info']['kontakt_info_telefon_data']?></p>

        </div>

    </div>

    <div class="kontakt-info-mail">

        <div class="kontakt-info-mail-img">
            <img src="<?php echo $kontakt['kontakt_info']['kontakt_info_mail_img'];?>"/>
        </div>

        <div class="kontakt-info-mail-text">

            <h2>E-mail</h2>

            <p class="kontakt-info-mail-text-desc"><?php echo $kontakt['kontakt_info']['kontakt_info_mail_desc']?></p>

            <p><?php echo $kontakt['kontakt_info']['kontakt_info_mail_data']?></p>

        </div>


    </div>

</section>

<!-- The beginning of section 'form' on kontakt page. -->
<section class="kontakt-form">

    <div class="kontakt-form-formularz">

        <h2>Formularz kontaktowy</h2>
        
        <?php echo do_shortcode( '[contact-form-7 id="411" title="Untitled"]' ); ?>        

    </div>

    <div class="kontakt-form-address">

        <div class="kontakt-form-address-img">

            <img src="<?php echo $kontakt['kontakt_form_img'];?>"/>

        </div>

        <div class="kontakt-form-address-data">

            <h3>Dane adresowe</h3>

            <p><?php echo $kontakt['kontakt_form_data'];?></p>

            <!-- Logo on the right bottom of the page. -->
            <img src="<?php echo $kontakt['kontakt_form_bottom'];?>"/>

        </div>

    </div>

</section>

<?php get_footer(); ?>