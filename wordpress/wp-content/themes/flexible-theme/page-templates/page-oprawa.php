<?php
/**
 * Template Name: Oprawa
 *
 */

 get_header();

?>

<?php $oprawa = get_field('oprawa');?>

<div class="wrapper">

<section class="oprawa" style="background-image: url('<?php echo $oprawa['oprawa_header_background'];?>')">

    <img class="img-oprawa" src="<?php echo $oprawa['img_oprawa'];?>"/>
    <div class="oprawa-header"><?php echo $oprawa['oprawa_header'];?></div>

</section>

<section class="oprawa-lines">

    <div class="line">

        <?php $line = 0;?>

        <?php foreach($oprawa['oprawa_line'] as $item): ?>

            <?php $line = $line + 1;?>

            <a href="#line-<?php echo $line;?>"  class="oprawa-line-case"><?php echo $item['line_header_1'];?></a>

        <?php endforeach;?>

    </div>

</section>

<section class="oprawa-main">

    <?php $count = 0;?>

    <?php foreach($oprawa['oprawa_line'] as $main):?>

        <?php $count = $count + 1;?>

        <?php if($count%2==0):?>

            <div class="oprawa-line parzysta" id="line-<?php echo $count?>">

                <div class="oprawa-line-text" style="top:-100px;" >

                    <h3><?php echo $main['line_header_1'];?></h3>
                    <div class="oprawa-line-text-desc"><?php echo $main['value_desc'];?></div>

                </div>

                <img class="oprawa-line-img"  src="<?php echo $main['value_img'];?>"/>
 
            </div>

        <?php else:?>

            <div class="oprawa-line nieparzysta" id="line-<?php echo $count;?>">

                <img class="oprawa-line-img"  src="<?php echo $main['value_img'];?>"/>
                
                <div class="oprawa-line-text">

                    <h3><?php echo $main['line_header_1'];?></h3>
                    <div class="oprawa-line-text-desc"><?php echo $main['value_desc'];?></div>

                </div>

            </div>

        <?php endif;?>

    <?php endforeach;?>

</section>

<?php get_footer(); ?>