<?php
/**
 * Template Name: Quality
 *
 */

 get_header();

?>

<div class="wrapper">
<?php $quality = get_field('quality_group');?>
<!-- Quality-top section with header and background image -->
<section class="quality" style="background-image: url('<?php echo $quality['quality_background'];?>')">
    
    <img class="img-quality" src="<?php echo $quality['img_quality']?>"/>
    <div class="quality-header"><?php echo $quality['quality_header'];?></div>

</section>

<!-- Content of quality page with image and some description to it + file -->
<section class="quality-content">

    
    <div class="quality-content-text">

        <h3><?php echo $quality['content_header'];?></h3>

        <?php echo $quality['content_text'];?>
        <a class="quality-content-file" href="<?php echo $quality['content_file'];?>"><p>TUTAJ</p></a>

        

    </div>

    <div class="quality-content-image">

        <img src="<?php echo $quality['content_image']?>"/>

    </div>

</section>

<?php get_footer(); ?>