<?php
/**
 * Template Name: About
 *
 */

 get_header();

?>

<div class="wrapper">
<?php $about = get_field('about_group');?>

<section class="about" style="background-image: url('<?php echo $about['about_background'];?>')">

    <div class="img-about"><img src="<?php echo $about['img_about'];?>"/></div>
    
    <div class="about-text">
        <h2 class="about-header"><?php echo $about['about_header'];?></h2>
    </div>
    

</section>

<section class="about-history">

    <div class="about-history-header"><?php echo $about['about_history_header'];?></div>

    <div class="about-history-start">Początek</div>

    <?php $hist_number = 0;?>

    <?php foreach($about['about_history_repeater'] as $history): ?>
        <?php $hist_number = $hist_number + 1;?>

        <?php if($hist_number%2==0):?>

            <div class="about-history-container even-history">

                <div class="about-history-image-date even-image-date">
                    <img class="about-history-img" src="<?php echo $history['about_history_img'];?>"/>
                    <h2 class="about-history-date"><?php echo $history['about_history_date'];?></h2>
                </div>

                <img class="image_1 even-img" src="<?php echo $history['image_1'];?>"/>
                <img class="image_2" src="<?php echo $history['image_2'];?>"/>
                

                <div class="about-history-text">

                    <h4 class="about-history-text-description"><?php echo $history['about_history_description'];?></h4>

                </div>
            </div>
        
        <?php else:?>

            <div class="about-history-container odd-history">

                <div class="about-history-image-date odd-image-date">
                    <img class="about-history-img" src="<?php echo $history['about_history_img'];?>"/>
                    <h2 class="about-history-date"><?php echo $history['about_history_date'];?></h2>
                </div>

                <img class="image_1 odd-img" src="<?php echo $history['image_1'];?>"/>
                <img class="image_2" src="<?php echo $history['image_2'];?>"/>

                <div class="about-history-text">

                    <h4 class="about-history-text-description"><?php echo $history['about_history_description'];?></h4>

                </div>
            </div>
        

        <?php endif;?>
    <?php endforeach;?>

    <div class="about-history-end">Dziś</div>

</section>

<section class="about-our">

    <div class="about-our-header"><?php echo $about['about_our_header'];?></div>

    <div class="about-all">

        <div class="about-our-values">

            <?php foreach($about['about_our_repeater'] as $value): ?>

                <div class="about-our-value-border">

                    <div class="about-our-value-text"><?php echo $value['about_our_value'];?></div>

                </div>

            <?php endforeach?>
            
        </div>

        <div class="about-our-description"><?php echo $about['about_our_description'];?></div>

    </div>
    
</section>

<section class="about-want" style="background-image: url('<?php echo $about['about_want']['about_want_background'];?>')">

    <div class="about-want-header"><?php echo $about['about_want']['about_want_header'];?></div>

</section>

<section class="about-want-later">

    <div class="about-want-values">

    <?php foreach($about['about_want_later']['about_want_repeater'] as $value): ?>

        <div class="about-want-value-border">

            <div class="about-want-value-text"><?php echo $value['about_want_value'];?></div>

        </div>

    <?php endforeach?>

    </div>

    <div class="about-want-value-1">

        <div class="about-want-value-1-header">
            <h4><?php echo $about['about_want_later']['about_want_header_1']?></h4>
        </div>

        <div class="about-want-value-1-desc">
            <p><?php echo $about['about_want_later']['about_want_desc_1']?></p>
        </div>

    </div>

    <div class="about-want-value-2">

        <div class="about-want-value-2-header">
            <h4><?php echo $about['about_want_later']['about_want_header_2']?></h4>
        </div>

        <div class="about-want-value-2-desc">
            <p><?php echo $about['about_want_later']['about_want_desc_2']?></p>
        </div>

    </div>

</section>

<section class="about-certificate">

    <div class="about-certificate-header"><?php echo $about['about_certificate_header'];?></div>

    <div class="about-certificate-img"><img src="<?php echo $about['about_certificate_img'];?>"/></div>

</section>


<?php get_footer(); ?>